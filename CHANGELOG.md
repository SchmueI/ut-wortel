Change Log
==========

HEAD
----

1.0.2
-----
- Add a refresh button

1.0.1
-----
- Remove unnecessary permissions

1.0.0
-----
- Initial release
